﻿using System.Threading.Tasks;

namespace Atlassian.Connect.TenantRepository
{
    public class Tenant
    {
        public Tenant(string clientKey, string baseUrl, string sharedSecret, bool enabled)
        {
            ClientKey = clientKey;
            BaseUrl = baseUrl;
            SharedSecret = sharedSecret;
            Enabled = enabled;
        }

        public string ClientKey { get; protected set; }
        public string BaseUrl { get; protected set; }
        public string SharedSecret { get; protected set; }
        public bool Enabled { get; protected set; }

        public void SetEnabled(bool enabled)
        {
            Enabled = enabled;
        }

        public Tenant WithEnabled(bool enabled)
        {
            return new Tenant(ClientKey, BaseUrl, SharedSecret, enabled);
        }
    }


	public interface ITenantRepository
	{
		Task AddTenant(Tenant tenant);
		Task<Tenant> GetTenant(string clientKey, bool enabledOnly);
        Task RemoveTenant(string clientKey);
        Task EnableTenant(string clientKey);
        Task DisableTenant(string clientKey);
    }
}
