﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Atlassian.Connect.TenantRepository
{

    public class JsonFileTenantRepository : ITenantRepository
    {
        private readonly string _path;
        private readonly SemaphoreSlim _instanceFileLock = new SemaphoreSlim(1);
        private readonly ConcurrentDictionary<string, Tenant> _inMemoryInstances = new ConcurrentDictionary<string, Tenant>();
        private readonly Task<ConcurrentDictionary<string, Tenant>> _inMemoryInstancesLazy;

        public JsonFileTenantRepository(string path)
        {
            _path = path;
            _inMemoryInstancesLazy = LoadJson();
        }

        private async Task<ConcurrentDictionary<string, Tenant>> LoadJson()
        {
            if (!File.Exists(_path))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_path));
                return new ConcurrentDictionary<string, Tenant>();
            }

            var bytes = await File.ReadAllBytesAsync(_path);

            var data = System.Text.Json.JsonSerializer.Deserialize<List<Tenant>>(bytes);

            return new ConcurrentDictionary<string, Tenant>(data.Select(d => new KeyValuePair<string, Tenant>(d.ClientKey, d)));
        }

        private async Task SaveJson()
        {
            var data = _inMemoryInstances.Values.Select(i => new Tenant(
                i.ClientKey,
                i.BaseUrl,
                i.SharedSecret,
                i.Enabled
            )).ToList();

            await _instanceFileLock.WaitAsync();
            try
            {
                await using var stream = File.Open(_path, FileMode.Create, FileAccess.Write);

                await System.Text.Json.JsonSerializer.SerializeAsync(stream, data);
            }
            finally
            {
                _instanceFileLock.Release();
            }
        }

        public async Task AddTenant(Tenant tenant)
        {
            (await _inMemoryInstancesLazy)[tenant.ClientKey] = tenant;
            await SaveJson();
        }

        public async Task<Tenant> GetTenant(string clientKey, bool enabledOnly)
        {
            if (!(await _inMemoryInstancesLazy).TryGetValue(clientKey, out var instance))
                return null;

            return enabledOnly && !instance.Enabled ? null : instance;
        }


        public async Task RemoveTenant(string clientKey)
        {
            (await _inMemoryInstancesLazy).TryRemove(clientKey, out _);
            await SaveJson();
        }

        public async Task EnableTenant(string clientKey)
        {
            var instanceDictionary = await _inMemoryInstancesLazy;
            if (!instanceDictionary.TryGetValue(clientKey, out var instance))
                return;

            instanceDictionary[clientKey] = instance.WithEnabled(true);
            await SaveJson();
        }

        public async Task DisableTenant(string clientKey)
        {
            var instanceDictionary = await _inMemoryInstancesLazy;
            if (!instanceDictionary.TryGetValue(clientKey, out var instance))
                return;

            instanceDictionary[clientKey] = instance.WithEnabled(false);
            await SaveJson();
        }
    }
}
