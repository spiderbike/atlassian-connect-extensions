﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;
using Microsoft.Extensions.Logging;

namespace Atlassian.Connect.TenantRepository
{
    public class AwsSecretsManagerTenantRepository : ITenantRepository
    {

        private readonly string _secretName;
        private readonly SemaphoreSlim _instanceFileLock = new SemaphoreSlim(1);

        private readonly Task<ConcurrentDictionary<string, Tenant>> _inMemoryInstancesLazy;
        private readonly AmazonSecretsManagerClient _client;

        public AwsSecretsManagerTenantRepository(string secretName, RegionEndpoint regionEndpoint)
        {
            _secretName = secretName;
            _client = new AmazonSecretsManagerClient(regionEndpoint);

            _inMemoryInstancesLazy = LoadJson();
        }

        private async Task<ConcurrentDictionary<string, Tenant>> LoadJson()
        {

            GetSecretValueRequest secretRequest = new GetSecretValueRequest
            {
                SecretId = _secretName
            };
            GetSecretValueResponse secretValue;
            try
            {
                secretValue = await _client.GetSecretValueAsync(secretRequest);
            }
            catch (ResourceNotFoundException exception)
            {
                //Secret does not exist so create.
                var createKeyRequest = new CreateSecretRequest
                {
                    SecretString = "[]",
                    Name = _secretName
                };
                await _client.CreateSecretAsync(createKeyRequest);
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }


            var data = System.Text.Json.JsonSerializer.Deserialize<List<Tenant>>(secretValue.SecretString);

            return new ConcurrentDictionary<string, Tenant>(data.Select(d => new KeyValuePair<string, Tenant>(d.ClientKey, d)));
        }

        private async Task SaveJson()
        {
            var data = (await _inMemoryInstancesLazy).Values.Select(t => new Tenant(t.ClientKey, t.BaseUrl, t.SharedSecret, t.Enabled)).ToList();

            await _instanceFileLock.WaitAsync();
            try
            {

                var updateSecretRequest = new UpdateSecretRequest
                {
                    SecretString = System.Text.Json.JsonSerializer.Serialize(data),
                    SecretId = _secretName
                };
                try
                {
                    await _client.UpdateSecretAsync(updateSecretRequest);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    throw;
                }
            }
            finally
            {
                _instanceFileLock.Release();
            }
        }


        public async Task AddTenant(Tenant tenant)
        {
            (await _inMemoryInstancesLazy)[tenant.ClientKey] = tenant;
            await SaveJson();
        }

        public async Task<Tenant> GetTenant(string clientKey, bool enabledOnly)
        {
            if (!(await _inMemoryInstancesLazy).TryGetValue(clientKey, out var instance)) 
                return null;

            return enabledOnly && !instance.Enabled? null : instance;
        }


        public async Task RemoveTenant(string clientKey)
        {
            (await _inMemoryInstancesLazy).TryRemove(clientKey, out _);
            await SaveJson();
        }

        public async Task EnableTenant(string clientKey)
        {
            var instanceDictionary = await _inMemoryInstancesLazy;
            if (!instanceDictionary.TryGetValue(clientKey, out var instance))
                return;

            instanceDictionary[clientKey] = instance.WithEnabled(true);
            await SaveJson();
        }

        public async Task DisableTenant(string clientKey)
        {
            var instanceDictionary = await _inMemoryInstancesLazy;
            if (!instanceDictionary.TryGetValue(clientKey, out var instance))
                return;

            instanceDictionary[clientKey] = instance.WithEnabled(false);
            await SaveJson();
        }
    }
}
