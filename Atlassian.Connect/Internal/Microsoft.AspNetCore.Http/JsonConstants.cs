﻿namespace Microsoft.AspNetCore.Http
{
    internal static class JsonConstants
    {
        public const string JsonContentType = "application/json";
        public const string JsonContentTypeWithCharset = "application/json; charset=utf-8";
    }
}