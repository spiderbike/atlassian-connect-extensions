﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using Atlassian.Connect.Internal;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace Atlassian.Connect
{
    public static class HttpContextExtensions
    {
        private static readonly string ItemsKey = $"AtlassianRequestContext-{Guid.NewGuid()}";

        public static void SetAtlassianRequestContext(this HttpContext context, AtlassianRequestContext.AtlassianRequestContext atlassianRequestContext)
        {
            context.Items[ItemsKey] = atlassianRequestContext;
        }

        public static AtlassianRequestContext.AtlassianRequestContext GetAtlassianRequestContext(this HttpContext context)
        {
            return (AtlassianRequestContext.AtlassianRequestContext)(context.Items.TryGetValue(ItemsKey, out var atlassianContext) ? atlassianContext : null);
        }

	}

	public static class RequestExtensions
	{
	    public static string BaseUrl(this HttpRequest request)
		{
			return GetProtocol(request) +
				"://" +
				GetHost(request) +
				request.PathBase.ToString().TrimEnd('/') + "/";
		}

		public static string GetHost(HttpRequest request)
		{
			if (!string.IsNullOrWhiteSpace(request.Headers["X-Original-Host"].ToString()))
			{
				return request.Headers["X-Original-Host"];
			}
			return request.Host.ToString();
		}

		public static string GetProtocol(HttpRequest request)
		{
			if (!string.IsNullOrWhiteSpace(request.Headers["X-Forwarded-Proto"].ToString()))
			{
				return request.Headers["X-Forwarded-Proto"];
			}
			return request.Scheme;
		}
	}
}