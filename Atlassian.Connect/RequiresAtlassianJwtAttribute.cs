using Microsoft.AspNetCore.Authorization;

namespace Test_Jira_Lambda.Controllers
{
    public class RequiresAtlassianJwtAttribute : AuthorizeAttribute
    {
        public static string PolicyName => "HasAtlassianJwt";

        public RequiresAtlassianJwtAttribute()
        {
            Policy = PolicyName;
        }
    }
}