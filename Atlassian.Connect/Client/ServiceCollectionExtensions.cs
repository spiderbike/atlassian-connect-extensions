﻿using System;
using System.Collections.Generic;
using System.Text;
using Atlassian.Connect.AtlassianRequestContext;
using Atlassian.Jira;
using Atlassian.Jira.Remote;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Atlassian.Connect.Client
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAtlassianConnectClient(this IServiceCollection services, Action<JiraRestClientSettings> configure = null)
        {
            services.AddHttpContextAccessor();

            services.AddScoped(provider =>
            {
                var settings = new JiraRestClientSettings();
                configure?.Invoke(settings);

                var atlassianRequestContext = provider.GetRequiredService<IHttpContextAccessor>().HttpContext.GetAtlassianRequestContext();

                return JiraJwtRestClient.CreateJwtRestClient(atlassianRequestContext.BaseUrl, atlassianRequestContext.AddonKey, atlassianRequestContext.ClientKey, atlassianRequestContext.SharedSecret, settings);
            });

            return services;
        }
    }
}
