﻿using Atlassian.Jira;
using Atlassian.Jira.Remote;

namespace Atlassian.Connect.Client
{
    public class JiraJwtRestClient : JiraRestClient
    {
        public static Jira.Jira CreateJwtRestClient(string url, string addonKey, string clientKey, string sharedSecret, JiraRestClientSettings settings = null)
        {
            settings ??= new JiraRestClientSettings();
            var restClient = new JiraJwtRestClient(url, addonKey, clientKey, sharedSecret, settings);

            return Jira.Jira.CreateRestClient(restClient, settings.Cache);
        }

        public JiraJwtRestClient(
            string url,
            string addonKey,
            string clientKey,
            string sharedSecret,
            JiraRestClientSettings settings)
            : base(
                url,
                new JwtAuthenticator(addonKey, clientKey, sharedSecret),
                settings)
        { }
    }
}