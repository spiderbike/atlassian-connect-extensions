﻿using System.Threading.Tasks;
using Atlassian.Connect.Lifecycle.Models;

namespace Atlassian.Connect.Lifecycle
{
    public interface IAppLifecycleManager
    {
        Task HandleEvent(AtlassianCallbackEventType eventType, LifecycleEventPayload eventPayload);
    }
}