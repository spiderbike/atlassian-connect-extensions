﻿using System;
using System.Threading.Tasks;
using Atlassian.Connect.Lifecycle.Models;
using Atlassian.Connect.TenantRepository;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Atlassian.Connect.Lifecycle
{
    public class AppLifecycleManager : IAppLifecycleManager
    {
        private readonly AtlassianConnectOptions _options;
        private readonly ITenantRepository _repository;
        private readonly ILogger<AppLifecycleManager> _logger;

        public AppLifecycleManager(IOptions<AtlassianConnectOptions> options, ITenantRepository repository, ILogger<AppLifecycleManager> logger)
        {
            _options = options.Value;
            _repository = repository;
            _logger = logger;
        }

        public async Task InstallPlugin(LifecycleEventPayload eventPayload)
        {
            _logger.LogDebug("running InstallPlugin");

            var ctx = new OnInstallContext
            {
                ClientKey = eventPayload.ClientKey,
                Event = eventPayload
            };

            if (_options.OnInstall != null)
            {
                await _options.OnInstall(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }

            await _repository.AddTenant(new Tenant(eventPayload.ClientKey, eventPayload.BaseUrl, eventPayload.SharedSecret, enabled: true));
        }

        public async Task UninstallPlugin(LifecycleEventPayload eventPayload)
        {
            _logger.LogDebug("running UninstallPlugin");

            var ctx = new OnUninstallContext
            {
                ClientKey = eventPayload.ClientKey,
                Event = eventPayload
            };

            if (_options.OnUninstall != null)
            {
                await _options.OnUninstall(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }

            await _repository.RemoveTenant(eventPayload.ClientKey);
        }

        public async Task EnablePlugin(LifecycleEventPayload eventPayload)
        {
            _logger.LogDebug("running EnablePlugin");

            var ctx = new OnEnabledContext
            {
                ClientKey = eventPayload.ClientKey,
                Event = eventPayload
            };

            if (_options.OnEnabled != null)
            {
                await _options.OnEnabled(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }

            await _repository.EnableTenant(eventPayload.ClientKey);
        }



        public async Task DisablePlugin(LifecycleEventPayload eventPayload)
        {
            _logger.LogDebug("running DisablePlugin");

            var ctx = new OnDisabledContext
            {
                ClientKey = eventPayload.ClientKey,
                Event = eventPayload
            };

            if (_options.OnDisabled != null)
            {
                await _options.OnDisabled(ctx);
            }

            if (ctx.PreventAction)
            {
                return;
            }

            
            
            await _repository.DisableTenant(eventPayload.ClientKey);
        }

        public Task HandleEvent(AtlassianCallbackEventType eventType, LifecycleEventPayload eventPayload)
        {
            if (eventPayload.EventType != eventType)
            {
                _logger.LogWarning("Event type from URL and payload differ: {urlEventType} != {payloadEventType}", eventType, eventPayload.EventType);
            }

            return eventType switch
            {
                AtlassianCallbackEventType.installed => InstallPlugin(eventPayload),
                AtlassianCallbackEventType.uninstalled => UninstallPlugin(eventPayload),
                AtlassianCallbackEventType.enabled => EnablePlugin(eventPayload),
                AtlassianCallbackEventType.disabled => DisablePlugin(eventPayload),
                _ => throw new ArgumentOutOfRangeException(nameof(eventType), eventType, $"Unknown event type: {eventType}")
            };
        }
    }

    public class OnInstallContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public LifecycleEventPayload Event { get; set; }
    }

    public class OnUninstallContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public LifecycleEventPayload Event { get; set; }
    }

    public class OnEnabledContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public LifecycleEventPayload Event { get; set; }
    }

    public class OnDisabledContext
    {
        public bool PreventAction { get; set; } = false;
        public string ClientKey { get; set; }
        public LifecycleEventPayload Event { get; set; }
    }
}
