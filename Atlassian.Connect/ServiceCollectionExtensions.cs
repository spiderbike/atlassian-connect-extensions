﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Atlassian.Connect.Lifecycle;
using Atlassian.Connect.TenantRepository;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Test_Jira_Lambda.Controllers;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using JwtConstants = Microsoft.IdentityModel.JsonWebTokens.JwtConstants;
using Microsoft.AspNetCore.Http;

namespace Atlassian.Connect
{
    /// <summary>
    /// Log messages and codes
    /// </summary>
    internal static class TokenLogMessages
    {
        // general
        // public const string IDX10000 = "IDX10000:";

        // properties, configuration
        public const string IDX10101 = "IDX10101: MaximumTokenSizeInBytes must be greater than zero. value: '{0}'";
        public const string IDX10100 = "IDX10100: ClockSkew must be greater than TimeSpan.Zero. value: '{0}'";
        public const string IDX10102 = "IDX10102: NameClaimType cannot be null or whitespace.";
        public const string IDX10103 = "IDX10103: RoleClaimType cannot be null or whitespace.";
        public const string IDX10104 = "IDX10104: TokenLifetimeInMinutes must be greater than zero. value: '{0}'";
        public const string IDX10105 = "IDX10105: ClaimValue that is a collection of collections is not supported. Such ClaimValue is found for ClaimType : '{0}'";

        // token validation
        public const string IDX10204 = "IDX10204: Unable to validate issuer. validationParameters.ValidIssuer is null or whitespace AND validationParameters.ValidIssuers is null.";
        public const string IDX10205 = "IDX10205: Issuer validation failed. Issuer: '{0}'. Did not match: validationParameters.ValidIssuer: '{1}' or validationParameters.ValidIssuers: '{2}'.";
        public const string IDX10206 = "IDX10206: Unable to validate audience. The 'audiences' parameter is empty.";
        public const string IDX10207 = "IDX10207: Unable to validate audience. The 'audiences' parameter is null.";
        public const string IDX10208 = "IDX10208: Unable to validate audience. validationParameters.ValidAudience is null or whitespace and validationParameters.ValidAudiences is null.";
        public const string IDX10209 = "IDX10209: Token has length: '{0}' which is larger than the MaximumTokenSizeInBytes: '{1}'.";
        public const string IDX10211 = "IDX10211: Unable to validate issuer. The 'issuer' parameter is null or whitespace";
        public const string IDX10214 = "IDX10214: Audience validation failed. Audiences: '{0}'. Did not match: validationParameters.ValidAudience: '{1}' or validationParameters.ValidAudiences: '{2}'.";
        public const string IDX10222 = "IDX10222: Lifetime validation failed. The token is not yet valid. ValidFrom: '{0}', Current time: '{1}'.";
        public const string IDX10223 = "IDX10223: Lifetime validation failed. The token is expired. ValidTo: '{0}', Current time: '{1}'.";
        public const string IDX10224 = "IDX10224: Lifetime validation failed. The NotBefore: '{0}' is after Expires: '{1}'.";
        public const string IDX10225 = "IDX10225: Lifetime validation failed. The token is missing an Expiration Time. Tokentype: '{0}'.";
        public const string IDX10227 = "IDX10227: TokenValidationParameters.TokenReplayCache is not null, indicating to check for token replay but the security token has no expiration time: token '{0}'.";
        public const string IDX10228 = "IDX10228: The securityToken has previously been validated, securityToken: '{0}'.";
        public const string IDX10229 = "IDX10229: TokenValidationParameters.TokenReplayCache was unable to add the securityToken: '{0}'.";
        public const string IDX10230 = "IDX10230: Lifetime validation failed. Delegate returned false, securitytoken: '{0}'.";
        public const string IDX10231 = "IDX10231: Audience validation failed. Delegate returned false, securitytoken: '{0}'.";
        public const string IDX10232 = "IDX10232: IssuerSigningKey validation failed. Delegate returned false, securityKey: '{0}'.";
        public const string IDX10233 = "IDX10233: ValidateAudience property on ValidationParameters is set to false. Exiting without validating the audience.";
        public const string IDX10234 = "IDX10234: Audience Validated.Audience: '{0}'";
        public const string IDX10235 = "IDX10235: ValidateIssuer property on ValidationParameters is set to false. Exiting without validating the issuer.";
        public const string IDX10236 = "IDX10236: Issuer Validated.Issuer: '{0}'";
        public const string IDX10237 = "IDX10237: ValidateIssuerSigningKey property on ValidationParameters is set to false. Exiting without validating the issuer signing key.";
        public const string IDX10238 = "IDX10238: ValidateLifetime property on ValidationParameters is set to false. Exiting without validating the lifetime.";
        public const string IDX10239 = "IDX10239: Lifetime of the token is valid.";
        public const string IDX10240 = "IDX10240: No token replay is detected.";
        public const string IDX10241 = "IDX10241: Security token validated. token: '{0}'.";
        public const string IDX10242 = "IDX10242: Security token: '{0}' has a valid signature.";
        public const string IDX10243 = "IDX10243: Reading issuer signing keys from validation parameters.";
        public const string IDX10244 = "IDX10244: Issuer is null or empty. Using runtime default for creating claims '{0}'.";
        public const string IDX10245 = "IDX10245: Creating claims identity from the validated token: '{0}'.";
        public const string IDX10246 = "IDX10246: ValidateTokenReplay property on ValidationParameters is set to false. Exiting without validating the token replay.";
        // public const string IDX10247 = "IDX10247:";
        public const string IDX10248 = "IDX10248: X509SecurityKey validation failed. The associated certificate is not yet valid. ValidFrom (UTC): '{0}', Current time (UTC): '{1}'.";
        public const string IDX10249 = "IDX10249: X509SecurityKey validation failed. The associated certificate has expired. ValidTo (UTC): '{0}', Current time (UTC): '{1}'.";
        public const string IDX10250 = "IDX10250: The associated certificate is valid. ValidFrom (UTC): '{0}', Current time (UTC): '{1}'.";
        public const string IDX10251 = "IDX10251: The associated certificate is valid. ValidTo (UTC): '{0}', Current time (UTC): '{1}'.";
        public const string IDX10252 = "IDX10252: RequireSignedTokens property on ValidationParameters is set to false and the issuer signing key is null. Exiting without validating the issuer signing key.";
        public const string IDX10253 = "IDX10253: RequireSignedTokens property on ValidationParameters is set to true, but the issuer signing key is null.";
        public const string IDX10254 = "IDX10254: '{0}.{1}' failed. The virtual method '{2}.{3}' returned null. If this method was overridden, ensure a valid '{4}' is returned.";
        public const string IDX10255 = "IDX10255: TypeValidator property on ValidationParameters is null and ValidTypes is either null or empty. Exiting without validating the token type.";
        public const string IDX10256 = "IDX10256: Unable to validate the token type. TokenValidationParameters.ValidTypes is set, but the 'typ' header claim is null or empty.";
        public const string IDX10257 = "IDX10257: Token type validation failed. Type: '{0}'. Did not match: validationParameters.TokenTypes: '{1}'.";
        public const string IDX10258 = "IDX10258: Token type validated. Type: '{0}'.";

        // 10500 - SignatureValidation
        public const string IDX10500 = "IDX10500: Signature validation failed. No security keys were provided to validate the signature.";
        public const string IDX10501 = "IDX10501: Signature validation failed. Unable to match key: \nkid: '{0}'.\nExceptions caught:\n '{1}'. \ntoken: '{2}'.";
        public const string IDX10503 = "IDX10503: Signature validation failed. Token does not have a kid. Keys tried: '{0}'.\nExceptions caught:\n '{1}'.\ntoken: '{2}'.";
        public const string IDX10504 = "IDX10504: Unable to validate signature, token does not have a signature: '{0}'.";
        public const string IDX10505 = "IDX10505: Signature validation failed. The user defined 'Delegate' specified on TokenValidationParameters returned null when validating token: '{0}'.";
        public const string IDX10506 = "IDX10506: Signature validation failed. The user defined 'Delegate' specified on TokenValidationParameters did not return a '{0}', but returned a '{1}' when validating token: '{2}'.";
        // public const string IDX10507 = "IDX10507:";
        public const string IDX10508 = "IDX10508: Signature validation failed. Signature is improperly formatted.";
        public const string IDX10509 = "IDX10509: Signature validation failed. The user defined 'Delegate' specified in TokenValidationParameters did not return a '{0}', but returned a '{1}' when reading token: '{2}'.";
        public const string IDX10510 = "IDX10510: Signature validation failed. The user defined 'Delegate' specified in TokenValidationParameters returned null when reading token: '{0}'.";
        public const string IDX10511 = "IDX10511: Signature validation failed. Keys tried: '{0}'. \nkid: '{1}'. \nExceptions caught:\n '{2}'.\ntoken: '{3}'.";
        public const string IDX10512 = "IDX10512: Signature validation failed. Token does not have KeyInfo. Keys tried: '{0}'.\nExceptions caught:\n '{1}'.\ntoken: '{2}'.";
        public const string IDX10513 = "IDX10513: Signature validation failed. Unable to match key: \nKeyInfo: '{0}'.\nExceptions caught:\n '{1}'. \ntoken: '{2}'.";
        public const string IDX10514 = "IDX10514: Signature validation failed. Keys tried: '{0}'. \nKeyInfo: '{1}'. \nExceptions caught:\n '{2}'.\ntoken: '{3}'.";
        public const string IDX10515 = "IDX10515: Signature validation failed. Unable to match key: \nKeyInfo: '{0}'.\nExceptions caught:\n '{1}'. \ntoken: '{2}'. Valid Lifetime: '{3}'. Valid Issuer: '{4}'";
        public const string IDX10516 = "IDX10516: Signature validation failed. Unable to match key: \nkid: '{0}'.\nExceptions caught:\n '{1}'. \ntoken: '{2}'. Valid Lifetime: '{3}'. Valid Issuer: '{4}'";

        // encryption / decryption
        // public const string IDX10600 = "IDX10600:";
        // public const string IDX10601 = "IDX10601:";
        public const string IDX10603 = "IDX10603: Decryption failed. Keys tried: '{0}'.\nExceptions caught:\n '{1}'.\ntoken: '{2}'";
        // public const string IDX10604 = "IDX10604:";
        // public const string IDX10605 = "IDX10605:";
        // public const string IDX10606 = "IDX10606:";
        public const string IDX10607 = "IDX10607: Decryption skipping key: '{0}', both validationParameters.CryptoProviderFactory and key.CryptoProviderFactory are null.";
        // public const string IDX10608 = "IDX10608:";
        public const string IDX10609 = "IDX10609: Decryption failed. No Keys tried: token: '{0}'.";
        public const string IDX10610 = "IDX10610: Decryption failed. Could not create decryption provider. Key: '{0}', Algorithm: '{1}'.";
        public const string IDX10611 = "IDX10611: Decryption failed. Encryption is not supported for: Algorithm: '{0}', SecurityKey: '{1}'.";
        public const string IDX10612 = "IDX10612: Decryption failed. Header.Enc is null or empty, it must be specified.";
        // public const string IDX10613 = "IDX10613:";
        // public const string IDX10614 = "IDX10614:";
        public const string IDX10615 = "IDX10615: Encryption failed. No support for: Algorithm: '{0}', SecurityKey: '{1}'.";
        public const string IDX10616 = "IDX10616: Encryption failed. EncryptionProvider failed for: Algorithm: '{0}', SecurityKey: '{1}'. See inner exception.";
        public const string IDX10617 = "IDX10617: Encryption failed. Keywrap is only supported for: '{0}', '{1}' and '{2}'. The content encryption specified is: '{3}'.";
        public const string IDX10618 = "IDX10618: Key unwrap failed using decryption Keys: '{0}'.\nExceptions caught:\n '{1}'.\ntoken: '{2}'.";
        public const string IDX10619 = "IDX10619: Decryption failed. Algorithm: '{0}'. Either the Encryption Algorithm: '{1}' or none of the Security Keys are supported by the CryptoProviderFactory.";
        public const string IDX10620 = "IDX10620: Unable to obtain a CryptoProviderFactory, both EncryptingCredentials.CryptoProviderFactory and EncryptingCredentials.Key.CrypoProviderFactory are null.";

        // Formating
        public const string IDX10400 = "IDX10400: Unable to decode: '{0}' as Base64url encoded string.";
        public const string IDX10401 = "IDX10401: Invalid requested key size. Valid key sizes are: 256, 384, and 512.";

        // Crypto Errors
        public const string IDX10621 = "IDX10621: '{0}' supports: '{1}' of types: '{2}' or '{3}'. SecurityKey received was of type '{4}'.";
        // public const string IDX10622 = "IDX10622:";
        // public const string IDX10623 = "IDX10623:";
        // public const string IDX10624 = "IDX10624:";
        // public const string IDX10627 = "IDX10627:";
        public const string IDX10628 = "IDX10628: Cannot set the MinimumSymmetricKeySizeInBits to less than '{0}'.";
        public const string IDX10630 = "IDX10630: The '{0}' for signing cannot be smaller than '{1}' bits. KeySize: '{2}'.";
        public const string IDX10631 = "IDX10631: The '{0}' for verifying cannot be smaller than '{1}' bits. KeySize: '{2}'.";
        public const string IDX10634 = "IDX10634: Unable to create the SignatureProvider.\nAlgorithm: '{0}', SecurityKey: '{1}'\n is not supported. The list of supported algorithms is available here: https://aka.ms/IdentityModel/supported-algorithms";
        // public const string IDX10635 = "IDX10635:";
        public const string IDX10636 = "IDX10636: CryptoProviderFactory.CreateForVerifying returned null for key: '{0}', signatureAlgorithm: '{1}'.";
        public const string IDX10637 = "IDX10637: CryptoProviderFactory.CreateForSigning returned null for key: '{0}', signatureAlgorithm: '{1}'.";
        public const string IDX10638 = "IDX10638: Cannot create the SignatureProvider, 'key.HasPrivateKey' is false, cannot create signatures. Key: {0}.";
        public const string IDX10640 = "IDX10640: Algorithm is not supported: '{0}'.";
        // public const string IDX10641 = "IDX10641:";
        public const string IDX10642 = "IDX10642: Creating signature using the input: '{0}'.";
        public const string IDX10643 = "IDX10643: Comparing the signature created over the input with the token signature: '{0}'.";
        // public const string IDX10644 = "IDX10644:";
        public const string IDX10645 = "IDX10645: Elliptical Curve not supported for curveId: '{0}'";
        public const string IDX10646 = "IDX10646: A CustomCryptoProvider was set and returned 'true' for IsSupportedAlgorithm(Algorithm: '{0}', Key: '{1}'), but Create.(algorithm, args) as '{2}' == NULL.";
        public const string IDX10647 = "IDX10647: A CustomCryptoProvider was set and returned 'true' for IsSupportedAlgorithm(Algorithm: '{0}'), but Create.(algorithm, args) as '{1}' == NULL.";
        // public const string IDX10648 = "IDX10648:";
        public const string IDX10649 = "IDX10649: Failed to create a SymmetricSignatureProvider for the algorithm '{0}'.";
        public const string IDX10650 = "IDX10650: Failed to verify ciphertext with aad '{0}'; iv '{1}'; and authenticationTag '{2}'.";
        // public const string IDX10651 = "IDX10651:";
        public const string IDX10652 = "IDX10652: The algorithm '{0}' is not supported.";
        public const string IDX10653 = "IDX10653: The encryption algorithm '{0}' requires a key size of at least '{1}' bits. Key '{2}', is of size: '{3}'.";
        public const string IDX10654 = "IDX10654: Decryption failed. Cryptographic operation exception: '{0}'.";
        public const string IDX10655 = "IDX10655: 'length' must be greater than 1: '{0}'";
        // public const string IDX10656 = "IDX10656:";
        public const string IDX10657 = "IDX10657: The SecurityKey provided for the symmetric key wrap algorithm cannot be converted to byte array. Type is: '{0}'.";
        public const string IDX10658 = "IDX10658: WrapKey failed, exception from cryptographic operation: '{0}'";
        public const string IDX10659 = "IDX10659: UnwrapKey failed, exception from cryptographic operation: '{0}'";
        // public const string IDX10660 = "IDX10660:";
        public const string IDX10661 = "IDX10661: Unable to create the KeyWrapProvider.\nKeyWrapAlgorithm: '{0}', SecurityKey: '{1}'\n is not supported.";
        public const string IDX10662 = "IDX10662: The KeyWrap algorithm '{0}' requires a key size of '{1}' bits. Key '{2}', is of size:'{3}'.";
        public const string IDX10663 = "IDX10663: Failed to create symmetric algorithm with SecurityKey: '{0}', KeyWrapAlgorithm: '{1}'.";
        public const string IDX10664 = "IDX10664: The length of input must be a multiple of 64 bits. The input size is: '{0}' bits.";
        public const string IDX10665 = "IDX10665: Data is not authentic";
        public const string IDX10666 = "IDX10666: Unable to create KeyedHashAlgorithm for algorithm '{0}'.";
        public const string IDX10667 = "IDX10667: Unable to obtain required byte array for KeyHashAlgorithm from SecurityKey: '{0}'.";
        public const string IDX10668 = "IDX10668: Unable to create '{0}', algorithm '{1}'; key: '{2}' is not supported.";
        public const string IDX10669 = "IDX10669: Failed to create symmetric algorithm.";
        // public const string IDX10670 = "IDX10670:";
        // public const string IDX10671 = "IDX10671:";
        // public const string IDX10672 = "IDX10672:";
        // public const string IDX10673 = "IDX10673:";
        public const string IDX10674 = "IDX10674: JsonWebKeyConverter does not support SecurityKey of type: {0}";
        public const string IDX10675 = "IDX10675: Cannot create a ECDsa object from the '{0}', the bytes from the decoded value of '{1}' must be less than the size associated with the curve: '{2}'. Size was: '{3}'.";
        // public const string IDX10676 = "IDX10676:";
        // public const string IDX10677 = "IDX10677:";
        // public const string IDX10678 = "IDX10678:";
        public const string IDX10679 = "IDX10679: Failed to decompress using algorithm '{0}'.";
        public const string IDX10680 = "IDX10680: Failed to compress using algorithm '{0}'.";
        // public const string IDX10681 = "IDX10681:";
        public const string IDX10682 = "IDX10682: Compression algorithm '{0}' is not supported.";
        // public const string IDX10683 = "IDX10683:";
        public const string IDX10684 = "IDX10684: Unable to convert the JsonWebKey to an AsymmetricSecurityKey. Algorithm: '{0}', Key: '{1}'.";
        public const string IDX10685 = "IDX10685: Unable to Sign, Internal SignFunction is not available.";
        public const string IDX10686 = "IDX10686: Unable to Verify, Internal VerifyFunction is not available.";
        public const string IDX10687 = "IDX10687: Unable to create a AsymmetricAdapter. For NET45 only types: '{0}' or '{1}' are supported. RSA is of type: '{2}'..";
        //public const string IDX10688 = "IDX10688:"
        public const string IDX10689 = "IDX10689: Unable to create an ECDsa object. See inner exception for more details.";
        public const string IDX10690 = "IDX10690: ECDsa creation is not supported by the current platform. For more details, see https://aka.ms/IdentityModel/create-ecdsa";
        //public const string IDX10691 = "IDX10691:"
        public const string IDX10692 = "IDX10692: The RSASS-PSS signature algorithm is not available on the .NET 4.5 target. The list of supported algorithms is available here: https://aka.ms/IdentityModel/supported-algorithms";
        public const string IDX10693 = "IDX10693: RSACryptoServiceProvider doesn't support the RSASSA-PSS signature algorithm. The list of supported algorithms is available here: https://aka.ms/IdentityModel/supported-algorithms";
        public const string IDX10694 = "IDX10694: JsonWebKeyConverter threw attempting to convert JsonWebKey: '{0}'. Exception: '{1}'.";
        public const string IDX10695 = "IDX10695: Unable to create a JsonWebKey from an ECDsa object. Required ECParameters structure is not supported by .NET Framework < 4.7.";
        public const string IDX10696 = "IDX10696: The algorithm '{0}' is not in the user-defined accepted list of algorithms.";
        public const string IDX10697 = "IDX10697: The user defined 'Delegate' AlgorithmValidator specified on TokenValidationParameters returned false when validating Algorithm: '{0}', SecurityKey: '{1}'.";
        public const string IDX10698 = "IDX10698: The SignatureProviderObjectPoolCacheSize must be greater than 0. Value: '{0}'.";
        public const string IDX10699 = "IDX10699: Unable to remove SignatureProvider with cache key: {0} from the InMemoryCryptoProviderCache. Exception: '{1}'.";

        // security keys
        public const string IDX10700 = "IDX10700: {0} is unable to use 'rsaParameters'. {1} is null.";
        //public const string IDX10701 = "IDX10701:"
        //public const string IDX10702 = "IDX10702:"
        public const string IDX10703 = "IDX10703: Cannot create a '{0}', key length is zero.";
        public const string IDX10704 = "IDX10704: Cannot verify the key size. The SecurityKey is not or cannot be converted to an AsymmetricSecuritKey. SecurityKey: '{0}'.";
        public const string IDX10705 = "IDX10705: Cannot create a JWK thumbprint, '{0}' is null or empty.";
        public const string IDX10706 = "IDX10706: Cannot create a JWK thumbprint, '{0}' must be one of the following: '{1}'.";
        public const string IDX10707 = "IDX10707: Cannot create a JSON representation of an asymmetric public key, '{0}' must be one of the following: '{1}'.";
        public const string IDX10708 = "IDX10708: Cannot create a JSON representation of an EC public key, '{0}' is null or empty.";
        public const string IDX10709 = "IDX10709: Cannot create a JSON representation of an RSA public key, '{0}' is null or empty.";
        public const string IDX10710 = "IDX10710: Computing a JWK thumbprint is supported only on SymmetricSecurityKey, JsonWebKey, RsaSecurityKey, X509SecurityKey, and ECDsaSecurityKey.";
        public const string IDX10711 = "IDX10711: Unable to Decrypt, Internal DecryptionFunction is not available.";
        public const string IDX10712 = "IDX10712: Unable to Encrypt, Internal EncryptionFunction is not available.";

        // Json specific errors
        //public const string IDX10801 = "IDX10801:"
        //public const string IDX10802 = "IDX10802:"
        //public const string IDX10804 = "IDX10804:"
        public const string IDX10805 = "IDX10805: Error deserializing json: '{0}' into '{1}'.";
        public const string IDX10806 = "IDX10806: Deserializing json: '{0}' into '{1}'.";
        //public const string IDX10807 = "IDX10807:"
        public const string IDX10808 = "IDX10808: The 'use' parameter of a JsonWebKey: '{0}' was expected to be 'sig' or empty, but was '{1}'.";
        //public const string IDX10809 = "IDX10809:"
        public const string IDX10810 = "IDX10810: Unable to convert the JsonWebKey: '{0}' to a X509SecurityKey, RsaSecurityKey or ECDSASecurityKey.";
        //public const string IDX10811 = "IDX10811:"
        public const string IDX10812 = "IDX10812: Unable to create a {0} from the properties found in the JsonWebKey: '{1}'.";
        public const string IDX10813 = "IDX10813: Unable to create a {0} from the properties found in the JsonWebKey: '{1}', Exception '{2}'.";

        //EventBasedLRUCache errors
        public const string IDX10900 = "IDX10900: EventBasedLRUCache._eventQueue encountered an error while processing a cache operation. Exception '{0}'.";
        public const string IDX10901 = "IDX10901: CryptoProviderCacheOptions.SizeLimit must be greater than 10. Value: '{0}'";
        public const string IDX10902 = "IDX10902: Object disposed exception in '{0}': '{1}'";

#pragma warning restore 1591
    }

    /// <summary>
    /// Log messages and codes
    /// </summary>
    internal static class LogMessages
    {
#pragma warning disable 1591
        // token creation
        internal const string IDX12401 = "IDX12401: Expires: '{0}' must be after NotBefore: '{1}'.";

        // JWT messages
        internal const string IDX12700 = "IDX12700: Error found while parsing date time. The '{0}' claim has value '{1}' which is could not be parsed to an integer.";
        internal const string IDX12701 = "IDX12701: Error found while parsing date time. The '{0}' claim has value '{1}' does not lie in the valid range.";
        internal const string IDX12706 = "IDX12706: '{0}' can only write SecurityTokens of type: '{1}', 'token' type is: '{2}'.";
        internal const string IDX12709 = "IDX12709: CanReadToken() returned false. JWT is not well formed: '{0}'.\nThe token needs to be in JWS or JWE Compact Serialization Format. (JWS): 'EncodedHeader.EndcodedPayload.EncodedSignature'. (JWE): 'EncodedProtectedHeader.EncodedEncryptedKey.EncodedInitializationVector.EncodedCiphertext.EncodedAuthenticationTag'.";
        internal const string IDX12710 = "IDX12710: Only a single 'Actor' is supported. Found second claim of type: '{0}', value: '{1}'";
        internal const string IDX12711 = "IDX12711: actor.BootstrapContext is not a string AND actor.BootstrapContext is not a JWT";
        internal const string IDX12712 = "IDX12712: actor.BootstrapContext is null. Creating the token using actor.Claims.";
        internal const string IDX12713 = "IDX12713: Creating actor value using actor.BootstrapContext(as string)";
        internal const string IDX12714 = "IDX12714: Creating actor value using actor.BootstrapContext.rawData";
        internal const string IDX12715 = "IDX12715: Creating actor value by writing the JwtSecurityToken created from actor.BootstrapContext";
        internal const string IDX12716 = "IDX12716: Decoding token: '{0}' into header, payload and signature.";
        internal const string IDX12720 = "IDX12720: Token string does not match the token formats: JWE (header.encryptedKey.iv.ciphertext.tag) or JWS (header.payload.signature)";
        internal const string IDX12721 = "IDX12721: Creating JwtSecurityToken: Issuer: '{0}', Audience: '{1}'";
        internal const string IDX12722 = "IDX12722: Creating security token from the header: '{0}', payload: '{1}' and raw signature: '{2}'.";
        internal const string IDX12723 = "IDX12723: Unable to decode the payload '{0}' as Base64Url encoded string. jwtEncodedString: '{1}'.";
        internal const string IDX12729 = "IDX12729: Unable to decode the header '{0}' as Base64Url encoded string. jwtEncodedString: '{1}'.";
        internal const string IDX12730 = "IDX12730: Failed to create the token encryption provider.";
        internal const string IDX12733 = "IDX12733: Unable to obtain a CryptoProviderFactory, both EncryptingCredentials.CryptoProviderFactory and EncryptingCredentials.Key.CrypoProviderFactory are both null.";
        internal const string IDX12735 = "IDX12735: If JwtSecurityToken.InnerToken != null, then JwtSecurityToken.Header.EncryptingCredentials must be set.";
        internal const string IDX12736 = "IDX12736: JwtSecurityToken.SigningCredentials is not supported when JwtSecurityToken.InnerToken is set.";
        internal const string IDX12737 = "IDX12737: EncryptingCredentials set on JwtSecurityToken.InnerToken is not supported.";
        internal const string IDX12738 = "IDX12738: Header.Cty != null, assuming JWS. Cty: '{0}'.";
        internal const string IDX12739 = "IDX12739: JWT: '{0}' has three segments but is not in proper JWS format.";
        internal const string IDX12740 = "IDX12740: JWT: '{0}' has five segments but is not in proper JWE format.";
        internal const string IDX12741 = "IDX12741: JWT: '{0}' must have three segments (JWS) or five segments (JWE).";
#pragma warning restore 1591
    }

    internal static class LoggingExtensions
    {
        private static Action<ILogger, Exception> _tokenValidationFailed;
        private static Action<ILogger, Exception> _tokenValidationSucceeded;
        private static Action<ILogger, Exception> _errorProcessingMessage;

        static LoggingExtensions()
        {
            _tokenValidationFailed = LoggerMessage.Define(
                eventId: new EventId(1, "TokenValidationFailed"),
                logLevel: LogLevel.Information,
                formatString: "Failed to validate the token.");
            _tokenValidationSucceeded = LoggerMessage.Define(
                eventId: new EventId(2, "TokenValidationSucceeded"),
                logLevel: LogLevel.Information,
                formatString: "Successfully validated the token.");
            _errorProcessingMessage = LoggerMessage.Define(
                eventId: new EventId(3, "ProcessingMessageFailed"),
                logLevel: LogLevel.Error,
                formatString: "Exception occurred while processing message.");
        }

        public static void TokenValidationFailed(this ILogger logger, Exception ex)
            => _tokenValidationFailed(logger, ex);

        public static void TokenValidationSucceeded(this ILogger logger)
            => _tokenValidationSucceeded(logger, null);

        public static void ErrorProcessingMessage(this ILogger logger, Exception ex)
            => _errorProcessingMessage(logger, ex);
    }


    public class AsyncJwtSecurityTokenHandler : JwtSecurityTokenHandler, IAsyncSecurityTokenValidator
    {
        public async Task<(ClaimsPrincipal, SecurityToken)> ValidateTokenAsync(string token, TokenValidationParameters validationParameters)
        {
            if (token.Length > MaximumTokenSizeInBytes)
                throw LogHelper.LogExceptionMessage(new ArgumentException(LogHelper.FormatInvariant(TokenLogMessages.IDX10209, token.Length, MaximumTokenSizeInBytes)));

            var tokenParts = token.Split(new char[] { '.' }, JwtConstants.MaxJwtSegmentCount + 1);

            if (tokenParts.Length != JwtConstants.JwsSegmentCount)
                throw LogHelper.LogExceptionMessage(new ArgumentException(LogHelper.FormatInvariant(LogMessages.IDX12741, token)));

            ClaimsPrincipal claimsPrincipal = null;
            SecurityToken signatureValidatedToken = null;

            signatureValidatedToken = await ValidateSignatureAsync(token, validationParameters);
            claimsPrincipal = ValidateTokenPayload(signatureValidatedToken as JwtSecurityToken, validationParameters);

            return (claimsPrincipal, signatureValidatedToken);
        }

        private async Task<SecurityToken> ValidateSignatureAsync(string token, TokenValidationParameters validationParameters)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw LogHelper.LogArgumentNullException(nameof(token));

            if (validationParameters == null)
                throw LogHelper.LogArgumentNullException(nameof(validationParameters));

            if (validationParameters.SignatureValidator != null)
            {
                var validatedJwtToken = validationParameters.SignatureValidator(token, validationParameters);
                if (validatedJwtToken == null)
                    throw LogHelper.LogExceptionMessage(new SecurityTokenInvalidSignatureException(LogHelper.FormatInvariant(TokenLogMessages.IDX10505, token)));

                var validatedJwt = validatedJwtToken as JwtSecurityToken;
                if (validatedJwt == null)
                    throw LogHelper.LogExceptionMessage(new SecurityTokenInvalidSignatureException(LogHelper.FormatInvariant(TokenLogMessages.IDX10506, typeof(JwtSecurityToken), validatedJwtToken.GetType(), token)));

                return validatedJwt;
            }

            JwtSecurityToken jwtToken = null;

            if (validationParameters.TokenReader != null)
            {
                var securityToken = validationParameters.TokenReader(token, validationParameters);
                if (securityToken == null)
                    throw LogHelper.LogExceptionMessage(new SecurityTokenInvalidSignatureException(LogHelper.FormatInvariant(TokenLogMessages.IDX10510, token)));

                jwtToken = securityToken as JwtSecurityToken;
                if (jwtToken == null)
                    throw LogHelper.LogExceptionMessage(new SecurityTokenInvalidSignatureException(LogHelper.FormatInvariant(TokenLogMessages.IDX10509, typeof(JwtSecurityToken), securityToken.GetType(), token)));
            }
            else
            {
                jwtToken = ReadJwtToken(token);
            }

            byte[] encodedBytes = Encoding.UTF8.GetBytes(jwtToken.RawHeader + "." + jwtToken.RawPayload);
            if (string.IsNullOrEmpty(jwtToken.RawSignature))
            {
                if (validationParameters.RequireSignedTokens)
                    throw LogHelper.LogExceptionMessage(new SecurityTokenInvalidSignatureException(LogHelper.FormatInvariant(TokenLogMessages.IDX10504, token)));
                else
                    return jwtToken;
            }

            bool kidMatched = false;
            IEnumerable<SecurityKey> keys = null;
            if (validationParameters is AsyncTokenValidationParameters asyncValidationParameters && asyncValidationParameters.IssuerSigningKeyResolverAsync != null)
            {
                keys = await asyncValidationParameters.IssuerSigningKeyResolverAsync(token, jwtToken, jwtToken.Header.Kid, validationParameters);
            }
            else if (validationParameters.IssuerSigningKeyResolver != null)
            {
                keys = validationParameters.IssuerSigningKeyResolver(token, jwtToken, jwtToken.Header.Kid, validationParameters);
            }
            else
            {
                var key = ResolveIssuerSigningKey(token, jwtToken, validationParameters);
                if (key != null)
                {
                    kidMatched = true;
                    keys = new List<SecurityKey> { key };
                }
            }

            if (keys == null && validationParameters.TryAllIssuerSigningKeys)
            {
                // control gets here if:
                // 1. User specified delegate: IssuerSigningKeyResolver returned null
                // 2. ResolveIssuerSigningKey returned null
                // Try all the keys. This is the degenerate case, not concerned about perf.
                keys = TokenUtilities.GetAllSigningKeys(validationParameters);
            }

            // keep track of exceptions thrown, keys that were tried
            var exceptionStrings = new StringBuilder();
            var keysAttempted = new StringBuilder();
            bool kidExists = !string.IsNullOrEmpty(jwtToken.Header.Kid);
            byte[] signatureBytes;

            try
            {
                signatureBytes = Base64UrlEncoder.DecodeBytes(jwtToken.RawSignature);
            }
            catch (FormatException e)
            {
                throw new SecurityTokenInvalidSignatureException(TokenLogMessages.IDX10508, e);
            }

            if (keys != null)
            {
                foreach (var key in keys)
                {
                    try
                    {
                        if (ValidateSignature(encodedBytes, signatureBytes, key, jwtToken.Header.Alg, jwtToken, validationParameters))
                        {
                            LogHelper.LogInformation(TokenLogMessages.IDX10242, token);
                            jwtToken.SigningKey = key;
                            return jwtToken;
                        }
                    }
                    catch (Exception ex)
                    {
                        exceptionStrings.AppendLine(ex.ToString());
                    }

                    if (key != null)
                    {
                        keysAttempted.AppendLine(key.ToString() + " , KeyId: " + key.KeyId);
                        if (kidExists && !kidMatched && key.KeyId != null)
                            kidMatched = jwtToken.Header.Kid.Equals(key.KeyId, key is X509SecurityKey ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal);
                    }
                }

            }

            if (kidExists)
            {
                if (kidMatched)
                    throw LogHelper.LogExceptionMessage(new SecurityTokenInvalidSignatureException(LogHelper.FormatInvariant(TokenLogMessages.IDX10511, keysAttempted, jwtToken.Header.Kid, exceptionStrings, jwtToken)));

                DateTime? expires = (jwtToken.Payload.Exp == null) ? null : new DateTime?(jwtToken.ValidTo);
                DateTime? notBefore = (jwtToken.Payload.Nbf == null) ? null : new DateTime?(jwtToken.ValidFrom);

                InternalValidators.ValidateLifetimeAndIssuerAfterSignatureNotValidatedJwt(
                    jwtToken,
                    notBefore,
                    expires,
                    jwtToken.Header.Kid,
                    validationParameters,
                    exceptionStrings);
            }

            if (keysAttempted.Length > 0)
                throw LogHelper.LogExceptionMessage(new SecurityTokenSignatureKeyNotFoundException(LogHelper.FormatInvariant(TokenLogMessages.IDX10503, keysAttempted, exceptionStrings, jwtToken)));

            throw LogHelper.LogExceptionMessage(new SecurityTokenSignatureKeyNotFoundException(TokenLogMessages.IDX10500));
        }

        /// <summary>
        /// Obtains a <see cref="SignatureProvider "/> and validates the signature.
        /// </summary>
        /// <param name="encodedBytes">Bytes to validate.</param>
        /// <param name="signature">Signature to compare against.</param>
        /// <param name="key"><See cref="SecurityKey"/> to use.</param>
        /// <param name="algorithm">Crypto algorithm to use.</param>
        /// <param name="securityToken">The <see cref="SecurityToken"/> being validated.</param>
        /// <param name="validationParameters">Priority will be given to <see cref="TokenValidationParameters.CryptoProviderFactory"/> over <see cref="SecurityKey.CryptoProviderFactory"/>.</param>
        /// <returns>'true' if signature is valid.</returns>
        private static bool ValidateSignature(byte[] encodedBytes, byte[] signature, SecurityKey key, string algorithm, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            Validators.ValidateAlgorithm(algorithm, key, securityToken, validationParameters);

            var cryptoProviderFactory = validationParameters.CryptoProviderFactory ?? key.CryptoProviderFactory;
            var signatureProvider = cryptoProviderFactory.CreateForVerifying(key, algorithm);
            if (signatureProvider == null)
                throw LogHelper.LogExceptionMessage(new InvalidOperationException(LogHelper.FormatInvariant(TokenLogMessages.IDX10636, key == null ? "Null" : key.ToString(), algorithm ?? "Null")));

            try
            {
                return signatureProvider.Verify(encodedBytes, signature);
            }
            finally
            {
                cryptoProviderFactory.ReleaseSignatureProvider(signatureProvider);
            }
        }
    }

    /// <summary>
    /// Validators meant to be kept internal
    /// </summary>
    internal static class InternalValidators
    {
        /// <summary>
        /// Called after signature validation has failed. Will always throw an exception.
        /// </summary>
        /// <exception cref="SecurityTokenSignatureKeyNotFoundException">
        /// If the lifetime and issuer are valid
        /// </exception>
        /// <exception cref="SecurityTokenUnableToValidateException">
        /// If the lifetime or issuer are invalid
        /// </exception>
        internal static void ValidateLifetimeAndIssuerAfterSignatureNotValidatedJwt(SecurityToken securityToken,
            DateTime? notBefore, DateTime? expires, string kid, TokenValidationParameters validationParameters,
            StringBuilder exceptionStrings)
        {
            bool validIssuer = false;
            bool validLifetime = false;

            try
            {
                Validators.ValidateLifetime(notBefore, expires, securityToken, validationParameters);
                validLifetime = true;
            }
            catch (Exception)
            {
                // validLifetime will remain false
            }

            try
            {
                Validators.ValidateIssuer(securityToken.Issuer, securityToken, validationParameters);
                validIssuer = true;
            }
            catch (Exception)
            {
                // validIssuer will remain false
            }

            if (validLifetime && validIssuer)
                throw LogHelper.LogExceptionMessage(new SecurityTokenSignatureKeyNotFoundException(
                    LogHelper.FormatInvariant(TokenLogMessages.IDX10501, kid, exceptionStrings, securityToken)));
            else
            {
                var validationFailure = ValidationFailure.None;

                if (!validLifetime)
                    validationFailure |= ValidationFailure.InvalidLifetime;

                if (!validIssuer)
                    validationFailure |= ValidationFailure.InvalidIssuer;

                throw LogHelper.LogExceptionMessage(new SecurityTokenUnableToValidateException(
                    validationFailure,
                    LogHelper.FormatInvariant(TokenLogMessages.IDX10516, kid, exceptionStrings, securityToken,
                        validLifetime, validIssuer)));
            }
        }
    }

    public class TokenUtilities
    {
        /// <summary>
        /// Returns all <see cref="SecurityKey"/> provided in validationParameters.
        /// </summary>
        /// <param name="validationParameters">A <see cref="TokenValidationParameters"/> required for validation.</param>
        /// <returns>Returns all <see cref="SecurityKey"/> provided in validationParameters.</returns>
        internal static IEnumerable<SecurityKey> GetAllSigningKeys(TokenValidationParameters validationParameters)
        {
            LogHelper.LogInformation(TokenLogMessages.IDX10243);
            if (validationParameters.IssuerSigningKey != null)
                yield return validationParameters.IssuerSigningKey;

            if (validationParameters.IssuerSigningKeys != null)
                foreach (SecurityKey key in validationParameters.IssuerSigningKeys)
                    yield return key;
        }
    }


    /// <summary>
    /// Provides an implementation of <see cref="JwtBearerHandler"/> that
    /// </summary>
    /// <remarks>
    /// This is a slightly refactored version of:
    /// https://github.com/dotnet/aspnetcore/blob/43a348b22468be9ed46ff1681cc113c9598b9f25/src/Security/Authentication/JwtBearer/src/JwtBearerHandler.cs
    /// </remarks>
    public class AsyncJwtBearerHandler : JwtBearerHandler
    {
        private OpenIdConnectConfiguration _configuration;

        public AsyncJwtBearerHandler(
            IOptionsMonitor<JwtBearerOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock
            )
            : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string token = null;
            try
            {
                // Give application opportunity to find from a different location, adjust, or reject token
                var messageReceivedContext = new MessageReceivedContext(Context, Scheme, Options);

                // event can set the token
                await Events.MessageReceived(messageReceivedContext);
                if (messageReceivedContext.Result != null)
                {
                    return messageReceivedContext.Result;
                }

                // If application retrieved token from somewhere else, use that.
                token = messageReceivedContext.Token;

                if (string.IsNullOrEmpty(token))
                {
                    string authorization = Request.Headers[HeaderNames.Authorization];

                    // If no authorization header found, nothing to process further
                    if (string.IsNullOrEmpty(authorization))
                    {
                        return AuthenticateResult.NoResult();
                    }

                    if (authorization.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
                    {
                        token = authorization.Substring("Bearer ".Length).Trim();
                    }

                    // If no token found, no further work possible
                    if (string.IsNullOrEmpty(token))
                    {
                        return AuthenticateResult.NoResult();
                    }
                }

                if (_configuration == null && Options.ConfigurationManager != null)
                {
                    _configuration = await Options.ConfigurationManager.GetConfigurationAsync(Context.RequestAborted);
                }

                var validationParameters = Options.TokenValidationParameters.Clone();
                if (_configuration != null)
                {
                    var issuers = new[] {_configuration.Issuer};
                    validationParameters.ValidIssuers = validationParameters.ValidIssuers?.Concat(issuers) ?? issuers;

                    validationParameters.IssuerSigningKeys =
                        validationParameters.IssuerSigningKeys?.Concat(_configuration.SigningKeys)
                        ?? _configuration.SigningKeys;
                }

                List<Exception> validationFailures = null;
                SecurityToken validatedToken;
                foreach (var validator in Options.SecurityTokenValidators)
                {
                    if (validator.CanReadToken(token))
                    {
                        ClaimsPrincipal principal;
                        try
                        {
                            if (validator is IAsyncSecurityTokenValidator asyncValidator)
                            {
                                (principal, validatedToken) = await asyncValidator
                                    .ValidateTokenAsync(token, validationParameters).ConfigureAwait(false);
                            }
                            else
                            {
                                principal = validator.ValidateToken(token, validationParameters, out validatedToken);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.TokenValidationFailed(ex);

                            // Refresh the configuration for exceptions that may be caused by key rollovers. The user can also request a refresh in the event.
                            if (Options.RefreshOnIssuerKeyNotFound && Options.ConfigurationManager != null
                                                                   && ex is SecurityTokenSignatureKeyNotFoundException)
                            {
                                Options.ConfigurationManager.RequestRefresh();
                            }

                            if (validationFailures == null)
                            {
                                validationFailures = new List<Exception>(1);
                            }

                            validationFailures.Add(ex);
                            continue;
                        }

                        Logger.TokenValidationSucceeded();

                        var tokenValidatedContext = new TokenValidatedContext(Context, Scheme, Options)
                        {
                            Principal = principal,
                            SecurityToken = validatedToken
                        };

                        await Events.TokenValidated(tokenValidatedContext);
                        if (tokenValidatedContext.Result != null)
                        {
                            return tokenValidatedContext.Result;
                        }

                        if (Options.SaveToken)
                        {
                            tokenValidatedContext.Properties.StoreTokens(new[]
                            {
                                new AuthenticationToken {Name = "access_token", Value = token}
                            });
                        }

                        tokenValidatedContext.Success();
                        return tokenValidatedContext.Result;
                    }
                }

                if (validationFailures != null)
                {
                    var authenticationFailedContext = new AuthenticationFailedContext(Context, Scheme, Options)
                    {
                        Exception = (validationFailures.Count == 1)
                            ? validationFailures[0]
                            : new AggregateException(validationFailures)
                    };

                    await Events.AuthenticationFailed(authenticationFailedContext);
                    if (authenticationFailedContext.Result != null)
                    {
                        return authenticationFailedContext.Result;
                    }

                    return AuthenticateResult.Fail(authenticationFailedContext.Exception);
                }

                return AuthenticateResult.Fail("No SecurityTokenValidator available for token: " + token ?? "[null]");
            }
            catch (Exception ex)
            {
                Logger.ErrorProcessingMessage(ex);

                var authenticationFailedContext = new AuthenticationFailedContext(Context, Scheme, Options)
                {
                    Exception = ex
                };

                await Events.AuthenticationFailed(authenticationFailedContext);
                if (authenticationFailedContext.Result != null)
                {
                    return authenticationFailedContext.Result;
                }

                throw;
            }
        }
    }

    public interface IAsyncSecurityTokenValidator : ISecurityTokenValidator
    {
        Task<(ClaimsPrincipal, SecurityToken)> ValidateTokenAsync(string token, TokenValidationParameters validationParameters);
    }

    /// <summary>Definition for IssuerSigningKeyResolver.</summary>
    /// <param name="token">The <see cref="T:System.String" /> representation of the token that is being validated.</param>
    /// <param name="securityToken">The <see cref="T:Microsoft.IdentityModel.Tokens.SecurityToken" /> that is being validated. It may be null.</param>
    /// <param name="kid">A key identifier. It may be null.</param>
    /// <param name="validationParameters"><see cref="T:Microsoft.IdentityModel.Tokens.TokenValidationParameters" /> required for validation.</param>
    /// <returns>A <see cref="T:Microsoft.IdentityModel.Tokens.SecurityKey" /> to use when validating a signature.</returns>
    public delegate Task<IEnumerable<SecurityKey>> IssuerSigningKeyResolverAsync(
        string token,
        SecurityToken securityToken,
        string kid,
        TokenValidationParameters validationParameters);

    public class AsyncTokenValidationParameters : TokenValidationParameters
    {
        public AsyncTokenValidationParameters()
        { }

        protected AsyncTokenValidationParameters(AsyncTokenValidationParameters other)
            : base(other)
        {
            if (other == null)
                throw LogHelper.LogExceptionMessage(new ArgumentNullException(nameof(other)));

            IssuerSigningKeyResolverAsync = other.IssuerSigningKeyResolverAsync;
        }

        public IssuerSigningKeyResolverAsync IssuerSigningKeyResolverAsync { get; set; }
        public override TokenValidationParameters Clone()
        {
            return new AsyncTokenValidationParameters(this);
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAtlassianConnectServices(this IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.ConfigureOptions<AtlassianConnectionOptionsConfigurator>();

            services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<JwtBearerOptions>, JwtBearerPostConfigureOptions>());

            services.AddOptions<JwtBearerOptions>(JwtBearerDefaults.AuthenticationScheme)
               .PostConfigure((JwtBearerOptions options, IOptions<AtlassianConnectOptions> atlassianConnectOptions, IHttpContextAccessor accessor) =>
               {
                   options.Events = new JwtBearerEvents
                   {
                       OnMessageReceived = context =>
                       {
                           context.Token = context.Request.Query["jwt"];
                           if (!String.IsNullOrEmpty(context.Token))
                               return Task.CompletedTask;

                           string authorization = context.Request.Headers[HeaderNames.Authorization];

                           if (string.IsNullOrEmpty(authorization))
                           {
                               context.NoResult();
                               return Task.CompletedTask;
                           }

                           if (authorization.StartsWith("JWT ", StringComparison.OrdinalIgnoreCase))
                           {
                               context.Token = authorization.Substring("JWT ".Length).Trim();
                           }

                           if (String.IsNullOrEmpty(context.Token))
                               context.NoResult();

                           return Task.CompletedTask;
                       },
                       OnTokenValidated = async context =>
                       {
                           var tenant = await context.HttpContext.RequestServices.GetRequiredService<ITenantRepository>().GetTenant(context.SecurityToken.Issuer, enabledOnly: false);

                           context.HttpContext.SetAtlassianRequestContext(new AtlassianRequestContext.AtlassianRequestContext(atlassianConnectOptions.Value.AddOnKey, tenant.ClientKey, tenant.SharedSecret, tenant.BaseUrl));
                       }
                   };

                   options.SecurityTokenValidators.Clear();
                   options.SecurityTokenValidators.Add(new AsyncJwtSecurityTokenHandler());

                   options.TokenValidationParameters = new AsyncTokenValidationParameters
                   {
                       IssuerSigningKeyResolverAsync = async (token, securityToken, kid, parameters) =>
                       {
                           var clientKey = securityToken.Issuer;
                           if (String.IsNullOrEmpty(clientKey))
                               return Enumerable.Empty<SecurityKey>();

                           var tenant = await accessor.HttpContext.RequestServices.GetRequiredService<ITenantRepository>().GetTenant(clientKey, enabledOnly: false);
                           if (tenant == null)
                               return Enumerable.Empty<SecurityKey>();

                           return new[] { new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tenant.SharedSecret)) };
                       },
                       ValidateAudience = false,
                       ValidateIssuer = false
                   };
               });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddScheme<JwtBearerOptions, AsyncJwtBearerHandler>(JwtBearerDefaults.AuthenticationScheme, displayName: null, _ => { });

            services.AddTransient<IAppLifecycleManager, AppLifecycleManager>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy(RequiresAtlassianJwtAttribute.PolicyName,
                    policy =>
                    {
                        policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                        policy.RequireAuthenticatedUser();
                    });
            });

            return services;
        }
    }

    public class AtlassianConnectionOptionsConfigurator : IPostConfigureOptions<AtlassianConnectOptions>
    {
        public void PostConfigure(string name, AtlassianConnectOptions options)
        {
            if (String.IsNullOrEmpty(options.Descriptor.Lifecycle?.Installed) && !String.IsNullOrEmpty(options.InstallCallbackPath))
            {
                options.Descriptor.Lifecycle ??= new Lifecycle.Models.Lifecycle();
                options.Descriptor.Lifecycle.Installed = options.InstallCallbackPath;
            }

            if (String.IsNullOrEmpty(options.Descriptor.Lifecycle?.Uninstalled) && !String.IsNullOrEmpty(options.UninstallCallbackPath))
            {
                options.Descriptor.Lifecycle ??= new Lifecycle.Models.Lifecycle();
                options.Descriptor.Lifecycle.Uninstalled = options.UninstallCallbackPath;
            }

            if (String.IsNullOrEmpty(options.Descriptor.Lifecycle?.Enabled) && !String.IsNullOrEmpty(options.EnabledCallbackPath))
            {
                options.Descriptor.Lifecycle ??= new Lifecycle.Models.Lifecycle();
                options.Descriptor.Lifecycle.Enabled = options.EnabledCallbackPath;
            }

            if (String.IsNullOrEmpty(options.Descriptor.Lifecycle?.Disabled) && !String.IsNullOrEmpty(options.DisabledCallbackPath))
            {
                options.Descriptor.Lifecycle ??= new Lifecycle.Models.Lifecycle();
                options.Descriptor.Lifecycle.Disabled = options.DisabledCallbackPath;
            }

            if (String.IsNullOrEmpty(options.Descriptor.Key) && !String.IsNullOrEmpty(options.AddOnKey))
            {
                options.Descriptor.Key = options.AddOnKey;
            }
        }
    }
}
