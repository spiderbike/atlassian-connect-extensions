﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Atlassian.Connect.Lifecycle;
using Atlassian.Connect.Lifecycle.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Atlassian.Connect
{
    public static class EndpointBuilderExtensions
    {
        public static void MapAtlassianLifecycleEndpoints(this IEndpointRouteBuilder endpoints)
        {
            var endpointOptions = endpoints.ServiceProvider.GetRequiredService<IOptions<AtlassianConnectOptions>>().Value;

            endpoints.Map(endpointOptions.InstallCallbackPath, context => HandleLifecycleEndpoint(context, AtlassianCallbackEventType.installed));
            endpoints.Map(endpointOptions.UninstallCallbackPath, context => HandleLifecycleEndpoint(context, AtlassianCallbackEventType.uninstalled));
            endpoints.Map(endpointOptions.EnabledCallbackPath, context => HandleLifecycleEndpoint(context, AtlassianCallbackEventType.enabled));
            endpoints.Map(endpointOptions.DisabledCallbackPath, context => HandleLifecycleEndpoint(context, AtlassianCallbackEventType.disabled));

            endpoints.Map(endpointOptions.DescriptorPath, async context =>
            {
                if (!HttpMethods.IsGet(context.Request.Method))
                {
                    context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                    return;
                }

                var descriptor = endpointOptions.Descriptor;

                if (descriptor.BaseUrl == null)
                {
                    descriptor = JsonSerializer.Deserialize<AddOnDescriptor>(JsonSerializer.Serialize(descriptor));
                    descriptor.BaseUrl = new Uri(context.Request.BaseUrl());
                }

                context.Response.StatusCode = (int)HttpStatusCode.OK;
                await context.Response.WriteAsJsonAsync(descriptor, new JsonSerializerOptions
                {
                    IgnoreNullValues = true,
                    Converters = { new JsonStringEnumConverter() }
                });
            });
        }

        private static async Task HandleLifecycleEndpoint(HttpContext context, AtlassianCallbackEventType eventType)
        {
            if (!HttpMethods.IsPost(context.Request.Method))
            {
                context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                return;
            }

            //using (var sr = new StreamReader(context.Request.Body, leaveOpen: true))
            //{
            //    var debugPayload = await sr.ReadToEndAsync();
            //}

            LifecycleEventPayload eventPayload;
            try
            {
                eventPayload = await context.Request.ReadFromJsonAsync<LifecycleEventPayload>(new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                    Converters = { new JsonStringEnumConverter() }
                });
            }
            catch (Exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return;
            }

            if (eventPayload != null)
            {
                var lifecycleManager = context.RequestServices.GetRequiredService<IAppLifecycleManager>();
                await lifecycleManager.HandleEvent(eventType, eventPayload);
                context.Response.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
        }
    }
}