namespace Atlassian.Connect.AtlassianRequestContext
{
    public class AtlassianRequestContext
    {
        public AtlassianRequestContext(string addonKey, string clientKey, string sharedSecret, string baseUrl)
        {
            AddonKey = addonKey;
            ClientKey = clientKey;
            SharedSecret = sharedSecret;
            BaseUrl = baseUrl;
        }

        public string AddonKey { get; }
        public string ClientKey { get; }
        public string SharedSecret { get; }
        public string BaseUrl { get; }
    }
}