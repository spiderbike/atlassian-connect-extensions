# External Webhooks

Instead of only responding to actions in Jira, sometimes you may want to setup an external webhook that caries out action on your Jira instance in response to an external system updating.


In this example we want a dropdown in Jira to update every time we add a new server in our AWS account.

Using the AWS event Bridge and a Lambda or similar we are able to trigger a webhook to be called each time a server is added in AWS.

Because the call to this webhook in our application will be triggered via an external source other than Jira, it will not contain the Jira JWT tokens required and will throw an error if the `[RequiresAtlassianJwt]` decorator is used (which we require in order to construct a Jira client, see [Getting Started Page](getting_started.md))

To get round this, we first build a controller that will create the Webhook URL for this installed instance, that contains the Client Key so we can then subsequently look up based on this once we get this requested from our external system.


## Create a Webhook URL for external system
```csharp
// GET api/webhook
[HttpGet]
[RequiresAtlassianJwt]
public async Task<IActionResult> Index()
{
    var tenant = HttpContext.GetAtlassianRequestContext(); 
    var baseUrl = tenant.BaseUrl;
#if DEBUG
    // Add this if using Ngrok for example locally and want to use the actual host.
    baseUrl = $"{HttpContext.Request.Headers["X-Forwarded-Proto"]}://{HttpContext.Request.Headers["X-Original-Host"]}";
#endif
    var webhookUrl = string.Format($"{baseUrl}{Request.Path}/{tenant.ClientKey}");

    ViewBag.webhookUrl = webhookUrl;

    return View();
}

```


## Create a Webhook URL

Then we can construct a Jira Client as below, and then modify it to how you wish according to the payload you expect to your webhook

*Note how we are retrieving the ClientKey that we constructed from the Method above*

```csharp
// GET api/webhook
[HttpGet("{clientKey}")]
public async Task<string> Get([FromRoute] string clientKey)
{
    var tenant = await _repository.GetTenant(clientKey, enabledOnly: false);

    var jira = JiraJwtRestClient.CreateJwtRestClient(tenant.BaseUrl, _options.AddOnKey, clientKey, tenant.SharedSecret);
    var issue = jira.CreateIssue("Test Project");
    issue.Type = "Incident";
    issue.Priority = "Minor";
    issue.Summary = $"New server created in AWS at {DateTime.Now}";

    await issue.SaveChangesAsync();

    return "Ok";

}
```

Obviously your implementation will be more complicated than adding a ticket each time a server is created, but this is the simplest example.