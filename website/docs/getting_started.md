# Getting Started

Start by creating a new .NET Core MVC project.

## Create a project in Visual studio

* Click `File\New\Project`
* Select **ASP.NET Core Web App (Model-View-Controller)**

## Create a project using the command line

* Create a new folder for your project
* Open a Windows Terminal in the new folder
* Enter the command `dotnet new mvc`

## Adding prerequisites  
* Add the Nuget package `Atlassian.Connect.Extensions` by selecting **Tools\Nuget Package Manager\Manage Nuget Packages for Solution**
* In the Startup.cs in the **app.UseEndpoints** section add `endpoints.MapAtlassianLifecycleEndpoints();` 

`In a new project the section will look similar to this`

```
app.UseEndpoints(endpoints =>
{
    endpoints.MapAtlassianLifecycleEndpoints();
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
});
```

* Add any References it recommends by clicking alert that pops up
* In the **ConfigureServices** method add in the following, for a similar example checkout the setup in [https://bitbucket.org/spiderbike/test-jira-lambda/src/master/Startup.cs](https://bitbucket.org/spiderbike/test-jira-lambda/src/master/Startup.cs)

```
services.AddAtlassianConnectServices();
services.AddAtlassianConnectClient();



services.AddOptions<AtlassianConnectOptions>()
    .Configure(opt =>
    {
        opt.InstallCallbackPath = "/installed";
        opt.UninstallCallbackPath = "/uninstalled";
        opt.EnabledCallbackPath = "/enabled";
        opt.DisabledCallbackPath = "/disabled";
        opt.DescriptorPath = "/atlassian-connect.json";
        opt.AddOnKey = "com.spiderbike.local.TestLambda";

        opt.Descriptor = new AddOnDescriptor
        {
            Name = "test jira lambda example",
            Description = "Atlassian Connect add-on, that shows the usage of data in attachments on your Jira instance over time.",
            Vendor = new Vendor() { Name = "Acme Software", Url = new Uri("http://www.acme.com") },
            Authentication = new Authentication
            {
                Type = AuthenticationType.jwt
            },
            ApiVersion = 1,
            EnableLicensing = true,
            Modules = new Modules
            {
                GeneralPages = new[]
                {
                    new PageModule
                    {
                        Url = "/api/values",
                        Key = "values-home",
                        Location = "system.top.navigation.bar",
                        Name = new I18nProperty {Value = "Values"}
                    }
                }
            },

            Scopes = new List<Scopes>
            {
                Scopes.read
            }
        };
    });

services.AddSingleton<ITenantRepository>(new AwsSecretsManagerTenantRepository($"{GetType().Namespace}-2", RegionEndpoint.EUWest1));
```

