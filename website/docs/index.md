# Home

## What is Atlassian.Connect.Extensions
This package is a fork of a project by Kevin Gysberg and Travis Smith, that allows creating of Jira Connect Applications using C# .NET Core.

These compliment existing frameworks for
* [Atlassian Connect for Node.js Express](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/)
* [Atlassian Connect for Spring Boot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot/src/master/)


## Package Features
* Strongly Typed creation of the attliassian-connect.json *app descriptor* [example](https://bitbucket.org/spiderbike/test-jira-lambda/src/24fdc90b9e92436dab6bcba5ff9223e5c5cf3b0b/Startup.cs#lines-38:128)
* Extensible support for storing credentials required for app installation, create a class that implements `ITenantRepository` to create your own Tenant Repository for storing application tokens
* Handling of the JWT token creation and authentication
* Easy creation of a Jira Client for API access 
  * [Example, setting up controller with a Jira Client](setting_up_controllers.md)
  * [Documentaion for Jira Client Project](https://bitbucket.org/farmas/atlassian.net-sdk/src/master/)


## Example Walkthrough Video

[YouTube: Creating a Jira Cloud App using C# .NET Core](https://www.youtube.com/watch?v=Ri9iqweDjLg)
