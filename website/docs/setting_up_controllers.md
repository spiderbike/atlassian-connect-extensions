# Setting up controllers

In your controller add the decorator 

`[RequiresAtlassianJwt]`

You can add this decorator either at the Controller class level, or on the individual methods

For all methods that are covered by this decorator, when they are requested it will try to verifiy the JWT token against the installed Tenant Repository. 

This is required to verify that Atlassian is the correct requestor, and not some external entity.


Because we have added the decorator `[RequiresAtlassianJwt]` we also have access to a Atlassian.Jira.Jira object for simplifying calling the Jira API

## Display the number of issues in Jira
```csharp
public class ValuesController : Controller
{

    private readonly Jira _jiraClient;

    public ValuesController(Jira jiraClient)
    {
        _jiraClient = jiraClient;
    }


    // GET api/values
    [HttpGet]
    public async Task<IActionResult> IndexAsync()
    {
        ViewBag.issueCount = (await _jiraClient.Issues.GetIssuesAsync()).Count; 
        return View();
    }
}

```

*Don't forget to add a reference to `Atlassian.Jira` in the top of the Controller class*




## Examples of using the Jira Client
```csharp
// use LINQ syntax to retrieve issues
var issues = from i in jira.Issues.Queryable
             where i.Assignee == "John" && i.Priority == "Critical"
             orderby i.Created
             select i;
```

By default, string comparisons are translated using the JIRA contains operator ('~'). A literal match can be forced by
wrapping the string with the LiteralMatch class:

```csharp
var issues = from i in jira.Issues.Queryable
             where i.Summary == new LiteralMatch("Hello world")
             select i;
```

## Create Issue

```csharp
var issue = jira.CreateIssue("Test Project");
issue.Type = "Incident";
issue.Priority = "Minor";
issue.Summary = "Data move";

await issue.SaveChangesAsync();
```

## Update Issue

```csharp
var issue = await jira.Issues.GetIssueAsync("TST-1");
issue.Summary = "Hello world 2";

await issue.SaveChangesAsync();
```


## Comments

```csharp
var issue = await jira.Issues.GetIssueAsync("TST-1");

// get comments
var comments = await issue.GetCommentsAsync();
Console.WriteLine(comments.First().Body);

```


Many more examples are available from the source maintaner available at [https://bitbucket.org/farmas/atlassian.net-sdk/src/master/](https://bitbucket.org/farmas/atlassian.net-sdk/src/master/) under the Documentation section.